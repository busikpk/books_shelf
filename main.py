import asyncio
import json
import os
import logging
import sys

from aiohttp.web import Application

from server import run

logging.basicConfig(
    format=u'%(levelname)-8s [%(asctime)s] [%(filename)s:%(lineno)s - %(funcName)2s ] %(message)s',
    level=logging.DEBUG,
    filename=u'books_shelf.log'
)

logger = logging.getLogger(__name__)


def _load_config(config_path):
    return json.loads(open(config_path).read())


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    app = Application()

    try:
        app['config'] = _load_config(os.environ.get('CONFIG_PATH', 'config.json'))
    except (IOError, TypeError) as err:
        logger.error('can\'t read config')
        sys.exit(1)

    logger.info('config successfully read')

    run(app, loop)
