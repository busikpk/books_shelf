import asyncio
import random
import time
import smtplib
import logging
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from .models_helper import book_pages_last_pk, match_pages_in_ids_range

logger = logging.getLogger(__name__)


_PAGE_ITERATION_STEP = 50
_SLEEP_MIN_SECONDS = 1
_SLEEP_MAX_SECONDS = 10


class Task:

    def __init__(self, loop, db, email_srv_config, email, search_query, ttl=0):
        self._loop = loop
        self._db = db
        self._email_srv_config = email_srv_config
        self._email = email
        self._search_query = search_query
        self._ttl = ttl
        self._checked_count = 0
        self._results = []

    def run(self):
        asyncio.ensure_future(self._run_task(), loop=self._loop)

    async def _run_task(self):
        """
            super slow search imitation
        """
        current_seq_value = 1
        start_time = time.time()

        while self._checked_count < current_seq_value and \
                (not self._ttl or start_time + self._ttl > time.time()):

            async with self._db.acquire() as c:
                current_seq_value = await book_pages_last_pk(c)

                prev_checked_count = self._checked_count
                self._checked_count += _PAGE_ITERATION_STEP

                pages = await match_pages_in_ids_range(c, prev_checked_count, self._checked_count, self._search_query)
                self._results.extend(pages)

            await asyncio.sleep(random.randint(_SLEEP_MIN_SECONDS, _SLEEP_MIN_SECONDS))

        logger.info(f'search duration {time.time() - start_time} for user: {self._email} query: {self._search_query}')

        await self._send_mail()

    async def _send_mail(self):
        try:
            server = smtplib.SMTP_SSL(self._email_srv_config['HOST'], self._email_srv_config['PORT'])
            server.ehlo()
            server.login(self._email_srv_config['USER'], self._email_srv_config['PASSWORD'])

            server.sendmail(self._email_srv_config['USER'], self._email, self._get_email_msg().as_string())
            server.close()
            logger.debug(f'mail sent for {self._email} with results for query: {self._search_query}')
        except Exception as err:
            logger.error(err)

    def _get_email_msg(self):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = 'Search results'
        msg['From'] = self._email_srv_config['USER']
        msg['To'] = self._email

        with open('email_template.txt', 'r') as f:
            template = ''.join(line for line in f)

        rows = ''.join(
            f"<tr><td>{row['name']}</td><td>{row['page']}</td><td>{row['text']}</td></tr>"
            for row in self._results
        )

        body = MIMEText(template.format(rows), 'html')
        msg.attach(body)
        return msg
