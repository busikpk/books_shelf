import logging
import io

from aiohttp.web import FileResponse, Response

from .models_helper import store_book
from .task import Task

logger = logging.getLogger(__name__)


async def index(request):
    return FileResponse('public/index.html')


async def upload_book(request):
    """
    only for testing
    """
    reader = await request.multipart()
    book = await reader.next()

    size = 0
    file_to_big = False
    buffer = io.BytesIO()

    try:
        while True:
            chunk = await book.read_chunk()

            if not chunk:
                break

            size += len(chunk)
            buffer.write(chunk)

            if size > 1024001:
                file_to_big = True
                break

        await store_book(request.app['db'], book.filename, buffer.getvalue())
    finally:
        buffer.close()

    if file_to_big:
        return Response(text='Book {} to big, max 1mb'.format(book.filename))

    return Response(text='Book {} stored'.format(book.filename))


async def search(request):
    data = await request.post()
    query = data.get('query', None)
    email = data.get('email', None)
    ttl = data.get('ttl', '')

    if not all([query, email, ttl.isdigit()]):
        return Response(text='Bad Request', status=400)

    app = request.app
    Task(app.loop, app['db'], app['config']['EMAIL'], email, query, int(ttl)).run()

    return Response(text=f'Search started!\nResults will sent to {email}')
