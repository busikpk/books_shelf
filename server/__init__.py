from aiohttp import web

from server import db
from server import routes


def run(app, loop):
    app.on_startup.append(routes.setup)
    app.on_startup.append(db.init)
    app.on_shutdown.append(db.close)

    server_config = app['config']['SERVER']

    web.run_app(app, host=server_config['HOST'], port=server_config['PORT'], loop=loop)
