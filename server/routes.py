from .views import index, upload_book, search

async def setup(app):
    app.router.add_get('/', index)
    app.router.add_post('/upload_book', upload_book)
    app.router.add_post('/search', search)
    app.router.add_static('/static', 'public')
