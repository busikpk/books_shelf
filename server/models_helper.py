from sqlalchemy.dialects.postgresql import insert
from sqlalchemy import select, and_, or_

from .models import book_page, book_page_seq


async def store_book(db, book_name, btext):
    pages = _divide_into_pages(book_name, btext)

    async with db.acquire() as c:
        await c.execute(insert(book_page).values(pages))


async def match_pages_in_ids_range(connection, left, right, search_query):
    query_pages_to_check = book_page.select().where(
        and_(
            book_page.c.id > left,
            book_page.c.id <= right
        )).lateral('pages_to_check')

    query = select([
        query_pages_to_check.c.id,
        query_pages_to_check.c.name,
        query_pages_to_check.c.page,
        query_pages_to_check.c.text,
    ]).select_from(query_pages_to_check).where(or_(
        query_pages_to_check.c.name.match(search_query),
        query_pages_to_check.c.text.match(search_query),
    ))

    res = await connection.execute(query)
    return [dict(zip(row.keys(), row.values())) for row in res]


async def book_pages_last_pk(connection):
    res = await connection.execute(f"select last_value from {book_page_seq.name}")
    row = [dict(zip(row.keys(), row.values())) for row in res]
    return row[0]['last_value']


def _divide_into_pages(book_name, text_bytes):
    buffer = bytearray()
    pages = []
    page = 1

    for char in text_bytes:
        buffer.append(char)
        if len(buffer) > 1000:
            pages.append({'name': book_name, 'page': page, 'text': buffer.decode('UTF-8')})
            buffer = bytearray()
            page += 1

    return pages
