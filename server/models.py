import sqlalchemy as sa


metadata = sa.MetaData()

book_page = sa.Table('book_page', metadata,
                     sa.Column('id', sa.Integer, primary_key=True),
                     sa.Column('name', sa.String(256), nullable=False),
                     sa.Column('page', sa.Integer, nullable=False),
                     sa.Column('text', sa.String(1024)))

book_page_seq = sa.Sequence('book_page_id_seq')
