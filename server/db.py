from aiopg.sa import create_engine

async def init(app):
    db_config = app['config']['DATABASE']

    app['db'] = await create_engine(
        user=db_config['USER'],
        database=db_config['NAME'],
        host=db_config['HOST'],
        password=db_config['PASSWORD'],
        port=db_config['PORT'],
        minsize=db_config['POOL_MIN_SIZE'],
        maxsize=db_config['POOL_MAX_SIZE']
    )

async def close(app):
    app['db'].close()
    await app['db'].wait_closed()
